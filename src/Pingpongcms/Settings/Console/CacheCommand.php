<?php

namespace Pingpongcms\Settings\Console;

use Illuminate\Console\Command;
use Pingpongcms\Settings\SettingRepository;

class CacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache the settings for better performance.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(SettingRepository $settings)
    {
        $this->call('setting:clear');

        $settings->cache();
        $this->info('Settings cached successfully!');
    }
}
