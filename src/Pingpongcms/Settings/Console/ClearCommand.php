<?php

namespace Pingpongcms\Settings\Console;

use Illuminate\Console\Command;
use Pingpongcms\Settings\SettingRepository;

class ClearCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all cached settings.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(SettingRepository $settings)
    {
        $settings->clear();
        $this->info('Settings cache cleared!');
    }
}
