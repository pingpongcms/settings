<?php

namespace Pingpongcms\Settings\Console;

use Illuminate\Console\Command;
use Pingpongcms\Settings\SettingRepository;

class SaveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save the settings as json file for better performance.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(SettingRepository $settings)
    {
        $settings->save();
        $this->info('Settings saved successfully!');
    }
}
