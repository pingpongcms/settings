<?php

namespace Pingpongcms\Settings\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pingpongcms\Settings\Setting;
use Pingpongcms\Settings\SettingRepository;

class SettingsController extends Controller
{
    public function index()
    {
        $this->authorize('update-settings');

        return view('settings::admin.index');
    }

    public function update(Request $request, SettingRepository $settings)
    {
        $this->authorize('update-settings');
        
        DB::transaction(function () use ($request) {
            foreach ($request->get('settings', []) as $key => $value) {
                if ($setting = Setting::key($key)->first()) {
                    $setting->update(compact('value'));
                } else {
                    Setting::create(compact('key', 'value'));
                }
            }
        });

        $settings->cache();
        $settings->save();

        return redirect()->back();
    }
}
