<?php

Route::get('settings', [
    'uses' => 'SettingsController@index',
    'as' => 'settings.index',
]);

Route::post('settings', [
    'uses' => 'SettingsController@update',
    'as' => 'settings.update',
]);
