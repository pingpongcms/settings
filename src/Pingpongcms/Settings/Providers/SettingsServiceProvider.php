<?php

namespace Pingpongcms\Settings\Providers;

use Illuminate\Support\ServiceProvider;
use Pingpongcms\Settings\Console\CacheCommand;
use Pingpongcms\Settings\Console\ClearCommand;
use Pingpongcms\Settings\Setting;
use Pingpongcms\Settings\Console\SaveCommand;
use Pingpongcms\Settings\SettingRepository;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../../migrations' => base_path('database/migrations')
        ], 'migrations');

        $this->loadTranslationsFrom(__DIR__ . '/../../../resources/lang', 'settings');

        $this->loadViewsFrom(realpath(__DIR__ . '/../../../resources/views'), 'settings');
    }

    protected function getSettingsFromConfig()
    {
        $settings = collect();
        
        foreach (config('settings', []) as $key => $value) {
            $settings->push(new Setting(compact('key', 'value')));
        }

        return $settings;
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('settings', function ($app) {
            try {
                return Setting::all();
            } catch (\Exception $e) {
                return $this->getSettingsFromConfig();
            }
        });

        $this->app->singleton('settings.repository', function ($app) {
            return new SettingRepository($app['cache.store'], $app['files']);
        });

        $this->app->singleton('settings.tabs', function () {
            return new \Pingpongcms\Settings\Tabs\TabCollection;
        });

        $this->commands([
            CacheCommand::class,
            ClearCommand::class,
            SaveCommand::class
        ]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
