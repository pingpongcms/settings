<?php

namespace Pingpongcms\Settings;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'key',
        'value',
    ];

    public function scopeKey($query, $key)
    {
        $query->whereKey($key);
    }

    public static function value($key, $default = null)
    {
        if ($setting = static::key($key)->first()) {
            return $setting->value;
        }

        return value($default);
    }
}
