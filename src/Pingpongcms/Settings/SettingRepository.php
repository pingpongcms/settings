<?php

namespace Pingpongcms\Settings;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Cache;

class SettingRepository
{
    protected $cache;
    
    protected $files;

    public function __construct(Repository $cache, Filesystem $files)
    {
        $this->cache = $cache;
        $this->files = $files;
    }

    public function clear()
    {
        $this->cache->forget('settings');

        if ($this->hasStorageFile()) {
            $this->clearStorageFile();
        }
    }

    public function clearStorageFile()
    {
        $this->files->delete($this->getStoragePath());
    }

    public function getStoragePath()
    {
        return storage_path('settings.json');
    }

    public function hasStorageFile()
    {
        return $this->files->exists($this->getStoragePath());
    }

    public function getFromStorageFile()
    {
        return collect(json_decode($this->files->get($this->getStoragePath())));
    }

    public function cache()
    {
        $this->cache->forever('settings', Setting::all());
    }

    public function save()
    {
        return $this->files->put($this->getStoragePath(), Setting::all()->toJson());
    }

    public function get($key, $default = null)
    {
        if ($this->hasStorageFile()) {
            $settings = $this->getFromStorageFile();
        } elseif ($this->cache->has("settings")) {
            $settings = $this->cache->get('settings');
        } else {
            $settings = app('settings');
        }

        $setting = $settings->where('key', $key)->first();

        return $setting ? $setting->value : value($default);
    }
}
