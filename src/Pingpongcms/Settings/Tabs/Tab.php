<?php

namespace Pingpongcms\Settings\Tabs;

class Tab
{
    public $title;

    public $slug;
    
    public $order;

    public $settings = [];

    public function __construct($title, $order = 0)
    {
        $this->title = $title;
        $this->slug = str_slug($title);
        $this->order = $order;
    }

    public function addSetting($label, $key, $type = 'text', $permission = 'auto')
    {
        $this->settings[] = (object) compact('label', 'key', 'type', 'permission');

        return $this;
    }

    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    public function order($order)
    {
        return $this->setOrder($order);
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
}
