<?php

namespace Pingpongcms\Settings\Tabs;

use Illuminate\Support\Collection;

class TabCollection extends Collection
{
    public $lastOrder = 0;

    public function add($title, callable $callback)
    {
        $this->push($tab = new Tab($title, $this->getOrder(null)));
        
        call_user_func($callback, $tab);

        $this->lastOrder = $this->sortBy('order', SORT_REGULAR, true)->first()->order;

        return $tab;
    }

    public function firstItem()
    {
        return $this->sortBy('order')->first();
    }

    public function isActive(Tab $tab)
    {
        return $tab->order == $this->firstItem()->order;
    }

    private function getOrder($order = null)
    {
        return $order ? $order : $this->lastOrder + 1;
    }

    public function getOrdered()
    {
        return $this->sortBy('order');
    }

    public function modify($title, callable $callback)
    {
        $tab = $this->where('title', $title)->first();

        call_user_func($callback, $tab);

        return $tab;
    }
}
