<?php

if (!function_exists('setting')) {
    function setting($key = null, $default = null)
    {
    	if (is_null($key)) {
    		return app('settings');
    	}
    	
        return app('settings.repository')->get($key, $default);
    }
}
