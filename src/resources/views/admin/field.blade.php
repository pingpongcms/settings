<div class="form-group">
    <label for="settings[{{ $setting->key }}]">{{ $setting->label }}</label>
    @if ($setting->type instanceof Closure)    
        {!! call_user_func($setting->type) !!}
    @elseif ($setting->type == 'textarea')
        {!! Form::textarea('settings['.$setting->key.']', setting($setting->key), ['class' => 'form-control']) !!}
    @else
        {!! Form::text('settings['.$setting->key.']', setting($setting->key), ['class' => 'form-control']) !!}
    @endif
</div>