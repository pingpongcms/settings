@extends('dashboard::layouts.master')

@section('content')
    
    <h1 class="page-header">Settings</h1>

    {!! Form::open(['files' => true]) !!}
        
        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                @foreach ($tabs as $tab)
                <li role="presentation" class=" {{ $tabs->isActive($tab) ? 'active' : '' }}">
                    <a href="#{{ $tab->slug }}" aria-controls="{{ $tab->slug }}" role="tab" data-toggle="tab">{{ $tab->title }}</a>
                </li>
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                @foreach ($tabs as $tab)
                <div role="tabpanel" class="tab-pane {{ $tabs->isActive($tab) ? 'active' : '' }}" id="{{ $tab->slug }}">
                    @foreach ($tab->settings as $setting)
                        @if ($setting->permission)
                            @if(Gate::allows($setting->permission == 'auto' ? 'update-'.$setting->key : $setting->permission))
                                @include('settings::admin.field')
                            @endif
                        @else
                            @include('settings::admin.field')    
                        @endif
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop